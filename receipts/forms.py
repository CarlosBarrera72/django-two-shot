from django import forms
from receipts.models import Receipt

class ReceiptForm(forms.ModelForm):
    vendor = forms.CharField(max_length=150)
    total = forms.DecimalField(max_digits=10, decimal_places=3)
    tax = forms.DecimalField(max_digits=10, decimal_places=3)
    date = forms.DateTimeField()

    class Meta:
        model = Receipt
        fields =[
            "vendor",
            "total",
            "tax",
            "date",
            "purchaser",
            "category",
            "account",
        ]
